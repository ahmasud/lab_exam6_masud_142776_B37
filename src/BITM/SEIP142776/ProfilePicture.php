<?php

namespace App;

use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $location;

    public function __construct()
    {

        parent::__construct();

    }

    public function index($Mode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from profile_picture');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if (array_key_exists('name',$data)){
            $this->name =$data['name'];
        }
        if (array_key_exists('file',$data)){
            $this->location =$data['file'];
        }
    }
    public  function store(){
        $sql = "INSERT INTO `profile_picture` (`name`, `location`) VALUES ('$this->name','$this->location')";
        echo $sql;

        $sth=$this->conn->prepare($sql);
        $sth->execute();

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Profile Pic: $this->location ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }

}// end of BookTitle class