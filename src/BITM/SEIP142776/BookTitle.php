<?php
namespace App;

use App\Database as DB;

use App\Message;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{

    public $STH;
    public $id;

    public $book_title;

    public $author_name;


    public function __construct()
    {

        parent::__construct();

    }


    public function index($Mode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from book_title');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if (array_key_exists('book_title',$data)){
            $this->book_title =$data['book_title'];
        }
        if (array_key_exists('author_name',$data)){
            $this->author_name =$data['author_name'];
        }
    }
public  function store(){
    $sql = "INSERT INTO `book_title` (`book_title`, `author_name`) VALUES ('$this->book_title','$this->author_name')";
    echo $sql;

    $sth=$this->conn->prepare($sql);
    $sth->execute();

    Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");


    Utility::redirect('create.php');

}

}



