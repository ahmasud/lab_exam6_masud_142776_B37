<?php
namespace App;

use PDO;
use PDOException;


class Database
{
    public $DBH;

    public $username ="root";
    public $password ="";
    public $db="exam_6_atomicproject_b37";
    public $host="localhost";

    public function __construct()
    {
        try {

            $this->DBH = new PDO('mysql:host=localhost;dbname=exam_6_atomicproject_b37',$this->username,$this->password);




        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

